const fs = require("fs");
const express = require("express");
const app = express();
const uuid = require('uuid');
const v4 = uuid.v4;

const port = 8000;

app.get('/', (request, response) => {
    response.status(200).send("Welcome to http-drill");
});

app.get('/html', (request, response) => {
    fs.readFile("./index.html", "utf-8", (error, html_data) => {
        if (error) {
            response.sendStatus(404);
        } else {
            response.status(200).send(html_data);
        }
    });
});

app.get('/json', (request, response) => {
    const json_data = {
        "slideshow": {
            "author": "Yours Truly",
            "date": "date of publication",
            "slides": [
                {
                    "title": "Wake up to WonderWidgets!",
                    "type": "all"
                },
                {
                    "items": [
                        "Why <em>WonderWidgets</em> are great",
                        "Who <em>buys</em> WonderWidgets"
                    ],
                    "title": "Overview",
                    "type": "all"
                }
            ],
            "title": "Sample Slide Show"
        }
    };

    response.status(200).send(JSON.stringify(json_data));
});

app.get('/uuid', (request, response) => {
    const uuid_data = {
        "uuid": v4()
    };

    response.status(200).send(JSON.stringify(uuid_data));
});

app.get('/status/:status_code', (request, response) => {
    const status = parseInt(request.params.status_code);

    if([100, 200, 300, 400, 500].includes(status)){
        response.sendStatus(200);
    }else{
        response.sendStatus(404);
    }
});

app.get('/delay/:delay_code', (request, response) => {
    const delay = parseInt(request.params.delay_code);

    setTimeout(() => {
        response.status(200).send(`successful ran after ${delay} second`);
    }, delay * 1000);
});

app.listen(port, () => {
    console.log(`listening on ${port}`);
});